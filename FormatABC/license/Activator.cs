﻿using Microsoft.Win32;
using System;
using System.Security.Cryptography;
using System.Text;

namespace FormatABC
{
    class Activator
    {

        private const string secret_key = "1ab6ce71372a5013b2abdbf89088670f";

        public const int WEEK_DEMO_VERSION = 604800;

        private const string url = "http://formatabc.ru/check_license.php";

        public Activator()
        {

        }

        public bool checkLicense(string email, string pass, string key)
        {
            string answer = Request.POST(url, "email=" + email + "&pass=" + pass + "&key=" + key);
            if (answer.Equals("true"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string generateKey(string email, string pass)
        {
            byte[] hash = Encoding.ASCII.GetBytes(email + pass + secret_key);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] hashenc = md5.ComputeHash(hash);
            string result = "";
            foreach (var b in hashenc)
            {
                result += b.ToString("x2");
            }
            return result;
        }

        public int readDemoDate()
        {
            try
            {
                RegistryKey reg = Registry.CurrentUser;
                reg = reg.OpenSubKey(@"Software\FormatABC", true);
                object getValue = reg.GetValue("demo_date");
                if (getValue == null)
                {
                    throw new RegeditValueLostException();
                }
                return Convert.ToInt32(getValue);
            }
            catch (NullReferenceException)
            {
                throw new RegeditValueLostException();
            }
        }

        public void writeDemoDateForFirstRun(int timeStamp)
        {
            RegistryKey regedit = Registry.CurrentUser;
            if (Registry.CurrentUser.OpenSubKey(@"Software\FormatABC") == null)
            {
                regedit = regedit.OpenSubKey("Software", true);
                regedit = regedit.CreateSubKey("FormatABC");
                regedit.SetValue("demo_date", timeStamp);
            }
            else
            {
                regedit = regedit.OpenSubKey(@"Software\FormatABC", true);
                regedit.SetValue("demo_date", timeStamp);
            }
        }

        public void writeLicense(string licenseHash)
        {
            RegistryKey regedit = Registry.CurrentUser;
            regedit = regedit.OpenSubKey(@"Software\FormatABC", true);
            regedit.SetValue("license_hash", licenseHash);
        }

        public void writeEmail(string email)
        {
            RegistryKey regedit = Registry.CurrentUser;
            regedit = regedit.OpenSubKey(@"Software\FormatABC", true);
            regedit.SetValue("email", email);
        }

        public void writePass(string pass)
        {
            RegistryKey regedit = Registry.CurrentUser;
            regedit = regedit.OpenSubKey(@"Software\FormatABC", true);
            regedit.SetValue("pass", pass);
        }

        public string readEmail()
        {
            try
            { 
                RegistryKey reg = Registry.CurrentUser;
                reg = reg.OpenSubKey(@"Software\FormatABC", true);
                return reg.GetValue("email").ToString();
            }
            catch (NullReferenceException)
            {
                throw new LicenseLostException();
            }
        }

        public string readPass()
        {
            try
            { 
            RegistryKey reg = Registry.CurrentUser;
            reg = reg.OpenSubKey(@"Software\FormatABC", true);
            return reg.GetValue("pass").ToString();
            }
            catch (NullReferenceException)
            {
                throw new LicenseLostException();
            }
        }

        public string readLicense()
        {
            try
            {
                RegistryKey regedit = Registry.CurrentUser;
                regedit = regedit.OpenSubKey(@"Software\FormatABC", true);
                return regedit.GetValue("license_hash").ToString();
            }
            catch (NullReferenceException)
            {
                throw new LicenseLostException();
            }
        }
    }
}
