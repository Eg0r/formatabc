<?php include ("header.php") ?>

	<div class="page_container_register">
		<div class="logo_header">
			<a href="/index.php"><img src="./images/abc.png" /></a>
		</div>
		<div id="id_page" class="page_register">
			<form class="form">
				<p>
					<label>Введите email:<br></label>
					<input class="input" id="email" type="text" size="15" maxlength="50">
				</p>
				<p>
					<label>Введите пароль:<br></label>
					<input class="input" id="pass" type="password" size="15" maxlength="20">
				</p>
				<p>
					<button type="button" class="button align_bottom button_red" onclick="register()"> Зарегистрироваться </button>
				</p>
				<p id="echo" style="display: none;">
				</p>
			</form>
		</div>
	</div>
	<script src="./js/position.js?5">
	</script>
	<script>
		function register()
		{
			$.ajax(
			{
			  url: './register.php',
			  type: "POST",
			  dataType: "text",
			  data: "email=" + $('#email').val() + "&pass=" + $('#pass').val(),
			  success: function(data, textStatus, jqXHR) 
			  {
				$('#echo').show();
				$('#echo').html(data);
			  }
			});
		}
	</script>
	<div class="footer simple_text center_text">
		2016 </br>
		Все права защищены
	</div>
</body>
</html>