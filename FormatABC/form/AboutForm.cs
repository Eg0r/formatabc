﻿using System.Windows.Forms;

namespace FormatABC
{
    public partial class AboutForm : Form
    {
        string text = @"<div style='text-align: justify; font-size: 12pt'><p align='center'><b>Format</b> <span style='color: #E36C0A'>A</span><span style='color: #AFA66D'>B</span><span style='color: #EDB200'>C</span></p>

<p>Программа предназначена для организаций и частных лиц, которым необходимо быстро и правильно отправить на печать файл, имеющий разные Форматы листов А0-А4. Программа автоматически определяет количество Форматов А0-А4 в файлах с расширением «.pdf» и отправляет на печать.</p>

<p>Версия: 1.0</br>
Автор идеи Ветошкин Дмитрий</br>
Технический разработчик Княжев Егор</br>
Email – FormatABC@yandex.ru
</p>


<p align='center'>Copyright ©2016</p></div>";

        public AboutForm()
        {
            InitializeComponent();

            webBrowser1.DocumentText = text;
        }
    }
}
