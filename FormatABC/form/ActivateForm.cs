﻿using System;
using System.Windows.Forms;

namespace FormatABC
{
    public partial class ActivateForm : Form
    {
        public ActivateForm()
        {
            InitializeComponent();
        }

        private void buttonActivate_Click(object sender, EventArgs e)
        {
            try
            {
                checkEmpty();
                Activator activator = new Activator();
                if (activator.checkLicense(textBoxLogin.Text, textBoxPass.Text, textBoxActivateKey.Text))
                {
                    MessageBox.Show("Программа успешно зарегистрирована", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    activator.writeEmail(textBoxLogin.Text);
                    activator.writePass(textBoxPass.Text);
                    activator.writeLicense(textBoxActivateKey.Text);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Неверные данные для лицензии", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (EmptyTextBoxException)
            {
                MessageBox.Show("Заполнены не все поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void checkEmpty()
        {
            if (textBoxLogin.Text.Equals("") || textBoxPass.Text.Equals("") || textBoxActivateKey.Text.Equals(""))
            {
                throw new EmptyTextBoxException();
            }
        }
    }
}
