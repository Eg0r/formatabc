﻿using System.IO; 
using System.Xml.Serialization;

namespace FormatABC
{
    public class Settings
    {
        private const string INI_FILE_NAME = "settings.ini";

        public IniObject iniObject;
        private string pathIniFile;

        public Settings(string pathIniFile)
        {
            this.iniObject = new IniObject();
            this.pathIniFile = pathIniFile;

            if (File.Exists(pathIniFile + "\\" + INI_FILE_NAME))
            {
                load();
            }
        }

        public void save()
        {
            using (Stream writer = new FileStream(pathIniFile + "\\" + INI_FILE_NAME, FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(IniObject));
                serializer.Serialize(writer, iniObject);
            }
        }

        public void load()
        {
            using (Stream stream = new FileStream(pathIniFile + "\\" + INI_FILE_NAME, FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(IniObject));
                iniObject = (IniObject)serializer.Deserialize(stream);
            }
        }
    }

    public class IniObject 
    {
        public bool isPrintA0;
        public string printeNameA0;
        public bool isPrintA1;
        public string printeNameA1;
        public bool isPrintA2;
        public string printeNameA2;
        public bool isPrintA3;
        public string printeNameA3;
        public bool isPrintA4;
        public string printeNameA4;
        public string adobeReaderPath;
    }
}
