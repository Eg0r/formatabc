﻿namespace FormatABC
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxPrinters = new System.Windows.Forms.ListBox();
            this.labelListBoxPrinters = new System.Windows.Forms.Label();
            this.buttonRefreshPrinters = new System.Windows.Forms.Button();
            this.buttonLoadPdfFile = new System.Windows.Forms.Button();
            this.openPdfFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.textBoxA1Printer = new System.Windows.Forms.TextBox();
            this.textBoxA2Printer = new System.Windows.Forms.TextBox();
            this.textBoxA3Printer = new System.Windows.Forms.TextBox();
            this.textBoxA4Printer = new System.Windows.Forms.TextBox();
            this.buttonToA1Printer = new System.Windows.Forms.Button();
            this.buttonToA2Printer = new System.Windows.Forms.Button();
            this.buttonToA3Printer = new System.Windows.Forms.Button();
            this.buttonToA4Printer = new System.Windows.Forms.Button();
            this.textBoxPageCountA1 = new System.Windows.Forms.TextBox();
            this.textBoxPageCountA2 = new System.Windows.Forms.TextBox();
            this.textBoxPageCountA3 = new System.Windows.Forms.TextBox();
            this.textBoxPageCountA4 = new System.Windows.Forms.TextBox();
            this.labelPageCount = new System.Windows.Forms.Label();
            this.buttonPrintPdfFile = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.путьКAdobeReaderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.активацияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.работаСПрограммойToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openAdobeReaderFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.textBoxPageCountA0 = new System.Windows.Forms.TextBox();
            this.buttonToA0Printer = new System.Windows.Forms.Button();
            this.textBoxA0Printer = new System.Windows.Forms.TextBox();
            this.checkBoxA0Printer = new System.Windows.Forms.CheckBox();
            this.checkBoxA1Printer = new System.Windows.Forms.CheckBox();
            this.checkBoxA2Printer = new System.Windows.Forms.CheckBox();
            this.checkBoxA3Printer = new System.Windows.Forms.CheckBox();
            this.checkBoxA4Printer = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxPrinters
            // 
            this.listBoxPrinters.FormattingEnabled = true;
            this.listBoxPrinters.Location = new System.Drawing.Point(12, 45);
            this.listBoxPrinters.Name = "listBoxPrinters";
            this.listBoxPrinters.Size = new System.Drawing.Size(256, 277);
            this.listBoxPrinters.TabIndex = 0;
            // 
            // labelListBoxPrinters
            // 
            this.labelListBoxPrinters.AutoSize = true;
            this.labelListBoxPrinters.Location = new System.Drawing.Point(12, 24);
            this.labelListBoxPrinters.Name = "labelListBoxPrinters";
            this.labelListBoxPrinters.Size = new System.Drawing.Size(61, 13);
            this.labelListBoxPrinters.TabIndex = 1;
            this.labelListBoxPrinters.Text = "Принтеры:";
            // 
            // buttonRefreshPrinters
            // 
            this.buttonRefreshPrinters.Location = new System.Drawing.Point(12, 328);
            this.buttonRefreshPrinters.Name = "buttonRefreshPrinters";
            this.buttonRefreshPrinters.Size = new System.Drawing.Size(116, 23);
            this.buttonRefreshPrinters.TabIndex = 2;
            this.buttonRefreshPrinters.Text = "Обновить список";
            this.buttonRefreshPrinters.UseVisualStyleBackColor = true;
            this.buttonRefreshPrinters.Click += new System.EventHandler(this.buttonRefreshPrinters_Click);
            // 
            // buttonLoadPdfFile
            // 
            this.buttonLoadPdfFile.Location = new System.Drawing.Point(155, 328);
            this.buttonLoadPdfFile.Name = "buttonLoadPdfFile";
            this.buttonLoadPdfFile.Size = new System.Drawing.Size(113, 23);
            this.buttonLoadPdfFile.TabIndex = 3;
            this.buttonLoadPdfFile.Text = "Загрузить файл";
            this.buttonLoadPdfFile.UseVisualStyleBackColor = true;
            this.buttonLoadPdfFile.Click += new System.EventHandler(this.buttonPrintPdfFile_Click);
            // 
            // openPdfFileDialog
            // 
            this.openPdfFileDialog.FileName = "openFileDialog1";
            // 
            // textBoxA1Printer
            // 
            this.textBoxA1Printer.Location = new System.Drawing.Point(289, 132);
            this.textBoxA1Printer.Name = "textBoxA1Printer";
            this.textBoxA1Printer.ReadOnly = true;
            this.textBoxA1Printer.Size = new System.Drawing.Size(253, 20);
            this.textBoxA1Printer.TabIndex = 4;
            // 
            // textBoxA2Printer
            // 
            this.textBoxA2Printer.Location = new System.Drawing.Point(289, 187);
            this.textBoxA2Printer.Name = "textBoxA2Printer";
            this.textBoxA2Printer.ReadOnly = true;
            this.textBoxA2Printer.Size = new System.Drawing.Size(253, 20);
            this.textBoxA2Printer.TabIndex = 5;
            // 
            // textBoxA3Printer
            // 
            this.textBoxA3Printer.Location = new System.Drawing.Point(289, 243);
            this.textBoxA3Printer.Name = "textBoxA3Printer";
            this.textBoxA3Printer.ReadOnly = true;
            this.textBoxA3Printer.Size = new System.Drawing.Size(253, 20);
            this.textBoxA3Printer.TabIndex = 6;
            // 
            // textBoxA4Printer
            // 
            this.textBoxA4Printer.Location = new System.Drawing.Point(289, 298);
            this.textBoxA4Printer.Name = "textBoxA4Printer";
            this.textBoxA4Printer.ReadOnly = true;
            this.textBoxA4Printer.Size = new System.Drawing.Size(253, 20);
            this.textBoxA4Printer.TabIndex = 7;
            // 
            // buttonToA1Printer
            // 
            this.buttonToA1Printer.Location = new System.Drawing.Point(289, 103);
            this.buttonToA1Printer.Name = "buttonToA1Printer";
            this.buttonToA1Printer.Size = new System.Drawing.Size(100, 23);
            this.buttonToA1Printer.TabIndex = 8;
            this.buttonToA1Printer.Text = "A1";
            this.buttonToA1Printer.UseVisualStyleBackColor = true;
            this.buttonToA1Printer.Click += new System.EventHandler(this.buttonToA1Printer_Click);
            // 
            // buttonToA2Printer
            // 
            this.buttonToA2Printer.Location = new System.Drawing.Point(289, 158);
            this.buttonToA2Printer.Name = "buttonToA2Printer";
            this.buttonToA2Printer.Size = new System.Drawing.Size(100, 23);
            this.buttonToA2Printer.TabIndex = 9;
            this.buttonToA2Printer.Text = "A2";
            this.buttonToA2Printer.UseVisualStyleBackColor = true;
            this.buttonToA2Printer.Click += new System.EventHandler(this.buttonToA2Printer_Click);
            // 
            // buttonToA3Printer
            // 
            this.buttonToA3Printer.Location = new System.Drawing.Point(289, 213);
            this.buttonToA3Printer.Name = "buttonToA3Printer";
            this.buttonToA3Printer.Size = new System.Drawing.Size(100, 23);
            this.buttonToA3Printer.TabIndex = 10;
            this.buttonToA3Printer.Text = "A3";
            this.buttonToA3Printer.UseVisualStyleBackColor = true;
            this.buttonToA3Printer.Click += new System.EventHandler(this.buttonToA3Printer_Click);
            // 
            // buttonToA4Printer
            // 
            this.buttonToA4Printer.Location = new System.Drawing.Point(289, 269);
            this.buttonToA4Printer.Name = "buttonToA4Printer";
            this.buttonToA4Printer.Size = new System.Drawing.Size(100, 23);
            this.buttonToA4Printer.TabIndex = 11;
            this.buttonToA4Printer.Text = "A4";
            this.buttonToA4Printer.UseVisualStyleBackColor = true;
            this.buttonToA4Printer.Click += new System.EventHandler(this.buttonToA4Printer_Click);
            // 
            // textBoxPageCountA1
            // 
            this.textBoxPageCountA1.Location = new System.Drawing.Point(573, 132);
            this.textBoxPageCountA1.Name = "textBoxPageCountA1";
            this.textBoxPageCountA1.ReadOnly = true;
            this.textBoxPageCountA1.Size = new System.Drawing.Size(111, 20);
            this.textBoxPageCountA1.TabIndex = 12;
            // 
            // textBoxPageCountA2
            // 
            this.textBoxPageCountA2.Location = new System.Drawing.Point(573, 187);
            this.textBoxPageCountA2.Name = "textBoxPageCountA2";
            this.textBoxPageCountA2.ReadOnly = true;
            this.textBoxPageCountA2.Size = new System.Drawing.Size(111, 20);
            this.textBoxPageCountA2.TabIndex = 13;
            // 
            // textBoxPageCountA3
            // 
            this.textBoxPageCountA3.Location = new System.Drawing.Point(573, 243);
            this.textBoxPageCountA3.Name = "textBoxPageCountA3";
            this.textBoxPageCountA3.ReadOnly = true;
            this.textBoxPageCountA3.Size = new System.Drawing.Size(111, 20);
            this.textBoxPageCountA3.TabIndex = 14;
            // 
            // textBoxPageCountA4
            // 
            this.textBoxPageCountA4.Location = new System.Drawing.Point(573, 298);
            this.textBoxPageCountA4.Name = "textBoxPageCountA4";
            this.textBoxPageCountA4.ReadOnly = true;
            this.textBoxPageCountA4.Size = new System.Drawing.Size(111, 20);
            this.textBoxPageCountA4.TabIndex = 15;
            // 
            // labelPageCount
            // 
            this.labelPageCount.AutoSize = true;
            this.labelPageCount.Location = new System.Drawing.Point(571, 45);
            this.labelPageCount.Name = "labelPageCount";
            this.labelPageCount.Size = new System.Drawing.Size(113, 13);
            this.labelPageCount.TabIndex = 16;
            this.labelPageCount.Text = "Количество страниц:";
            // 
            // buttonPrintPdfFile
            // 
            this.buttonPrintPdfFile.Location = new System.Drawing.Point(573, 328);
            this.buttonPrintPdfFile.Name = "buttonPrintPdfFile";
            this.buttonPrintPdfFile.Size = new System.Drawing.Size(110, 23);
            this.buttonPrintPdfFile.TabIndex = 17;
            this.buttonPrintPdfFile.Text = "Печать";
            this.buttonPrintPdfFile.UseVisualStyleBackColor = true;
            this.buttonPrintPdfFile.Click += new System.EventHandler(this.buttonPrintPdfFile_Click_1);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.настройкиToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(704, 24);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.путьКAdobeReaderToolStripMenuItem,
            this.активацияToolStripMenuItem});
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.настройкиToolStripMenuItem.Text = "Настройки";
            // 
            // путьКAdobeReaderToolStripMenuItem
            // 
            this.путьКAdobeReaderToolStripMenuItem.Name = "путьКAdobeReaderToolStripMenuItem";
            this.путьКAdobeReaderToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.путьКAdobeReaderToolStripMenuItem.Text = "Путь к AdobeReader";
            this.путьКAdobeReaderToolStripMenuItem.Click += new System.EventHandler(this.путьКAdobeReaderToolStripMenuItem_Click);
            // 
            // активацияToolStripMenuItem
            // 
            this.активацияToolStripMenuItem.Name = "активацияToolStripMenuItem";
            this.активацияToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.активацияToolStripMenuItem.Text = "Активация";
            this.активацияToolStripMenuItem.Click += new System.EventHandler(this.активацияToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.работаСПрограммойToolStripMenuItem,
            this.оПрограммеToolStripMenuItem});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // работаСПрограммойToolStripMenuItem
            // 
            this.работаСПрограммойToolStripMenuItem.Name = "работаСПрограммойToolStripMenuItem";
            this.работаСПрограммойToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.работаСПрограммойToolStripMenuItem.Text = "Работа с программой";
            this.работаСПрограммойToolStripMenuItem.Click += new System.EventHandler(this.работаСПрограммойToolStripMenuItem_Click);
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem_Click);
            // 
            // textBoxPageCountA0
            // 
            this.textBoxPageCountA0.Location = new System.Drawing.Point(573, 74);
            this.textBoxPageCountA0.Name = "textBoxPageCountA0";
            this.textBoxPageCountA0.ReadOnly = true;
            this.textBoxPageCountA0.Size = new System.Drawing.Size(111, 20);
            this.textBoxPageCountA0.TabIndex = 21;
            // 
            // buttonToA0Printer
            // 
            this.buttonToA0Printer.Location = new System.Drawing.Point(289, 45);
            this.buttonToA0Printer.Name = "buttonToA0Printer";
            this.buttonToA0Printer.Size = new System.Drawing.Size(100, 23);
            this.buttonToA0Printer.TabIndex = 20;
            this.buttonToA0Printer.Text = "A0";
            this.buttonToA0Printer.UseVisualStyleBackColor = true;
            this.buttonToA0Printer.Click += new System.EventHandler(this.buttonToA0Printer_Click);
            // 
            // textBoxA0Printer
            // 
            this.textBoxA0Printer.Location = new System.Drawing.Point(289, 74);
            this.textBoxA0Printer.Name = "textBoxA0Printer";
            this.textBoxA0Printer.ReadOnly = true;
            this.textBoxA0Printer.Size = new System.Drawing.Size(253, 20);
            this.textBoxA0Printer.TabIndex = 19;
            // 
            // checkBoxA0Printer
            // 
            this.checkBoxA0Printer.AutoSize = true;
            this.checkBoxA0Printer.Location = new System.Drawing.Point(413, 50);
            this.checkBoxA0Printer.Name = "checkBoxA0Printer";
            this.checkBoxA0Printer.Size = new System.Drawing.Size(15, 14);
            this.checkBoxA0Printer.TabIndex = 22;
            this.checkBoxA0Printer.UseVisualStyleBackColor = true;
            this.checkBoxA0Printer.CheckedChanged += new System.EventHandler(this.checkBoxA0Printer_CheckedChanged);
            // 
            // checkBoxA1Printer
            // 
            this.checkBoxA1Printer.AutoSize = true;
            this.checkBoxA1Printer.Location = new System.Drawing.Point(413, 108);
            this.checkBoxA1Printer.Name = "checkBoxA1Printer";
            this.checkBoxA1Printer.Size = new System.Drawing.Size(15, 14);
            this.checkBoxA1Printer.TabIndex = 23;
            this.checkBoxA1Printer.UseVisualStyleBackColor = true;
            this.checkBoxA1Printer.CheckedChanged += new System.EventHandler(this.checkBoxA1Printer_CheckedChanged);
            // 
            // checkBoxA2Printer
            // 
            this.checkBoxA2Printer.AutoSize = true;
            this.checkBoxA2Printer.Location = new System.Drawing.Point(413, 163);
            this.checkBoxA2Printer.Name = "checkBoxA2Printer";
            this.checkBoxA2Printer.Size = new System.Drawing.Size(15, 14);
            this.checkBoxA2Printer.TabIndex = 24;
            this.checkBoxA2Printer.UseVisualStyleBackColor = true;
            // 
            // checkBoxA3Printer
            // 
            this.checkBoxA3Printer.AutoSize = true;
            this.checkBoxA3Printer.Location = new System.Drawing.Point(413, 218);
            this.checkBoxA3Printer.Name = "checkBoxA3Printer";
            this.checkBoxA3Printer.Size = new System.Drawing.Size(15, 14);
            this.checkBoxA3Printer.TabIndex = 25;
            this.checkBoxA3Printer.UseVisualStyleBackColor = true;
            // 
            // checkBoxA4Printer
            // 
            this.checkBoxA4Printer.AutoSize = true;
            this.checkBoxA4Printer.Location = new System.Drawing.Point(413, 274);
            this.checkBoxA4Printer.Name = "checkBoxA4Printer";
            this.checkBoxA4Printer.Size = new System.Drawing.Size(15, 14);
            this.checkBoxA4Printer.TabIndex = 26;
            this.checkBoxA4Printer.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 359);
            this.Controls.Add(this.checkBoxA4Printer);
            this.Controls.Add(this.checkBoxA3Printer);
            this.Controls.Add(this.checkBoxA2Printer);
            this.Controls.Add(this.checkBoxA1Printer);
            this.Controls.Add(this.checkBoxA0Printer);
            this.Controls.Add(this.textBoxPageCountA0);
            this.Controls.Add(this.buttonToA0Printer);
            this.Controls.Add(this.textBoxA0Printer);
            this.Controls.Add(this.buttonPrintPdfFile);
            this.Controls.Add(this.labelPageCount);
            this.Controls.Add(this.textBoxPageCountA4);
            this.Controls.Add(this.textBoxPageCountA3);
            this.Controls.Add(this.textBoxPageCountA2);
            this.Controls.Add(this.textBoxPageCountA1);
            this.Controls.Add(this.buttonToA4Printer);
            this.Controls.Add(this.buttonToA3Printer);
            this.Controls.Add(this.buttonToA2Printer);
            this.Controls.Add(this.buttonToA1Printer);
            this.Controls.Add(this.textBoxA4Printer);
            this.Controls.Add(this.textBoxA3Printer);
            this.Controls.Add(this.textBoxA2Printer);
            this.Controls.Add(this.textBoxA1Printer);
            this.Controls.Add(this.buttonLoadPdfFile);
            this.Controls.Add(this.buttonRefreshPrinters);
            this.Controls.Add(this.labelListBoxPrinters);
            this.Controls.Add(this.listBoxPrinters);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Расширенная печать .pdf";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxPrinters;
        private System.Windows.Forms.Label labelListBoxPrinters;
        private System.Windows.Forms.Button buttonRefreshPrinters;
        private System.Windows.Forms.Button buttonLoadPdfFile;
        private System.Windows.Forms.OpenFileDialog openPdfFileDialog;
        private System.Windows.Forms.TextBox textBoxA1Printer;
        private System.Windows.Forms.TextBox textBoxA2Printer;
        private System.Windows.Forms.TextBox textBoxA3Printer;
        private System.Windows.Forms.TextBox textBoxA4Printer;
        private System.Windows.Forms.Button buttonToA1Printer;
        private System.Windows.Forms.Button buttonToA2Printer;
        private System.Windows.Forms.Button buttonToA3Printer;
        private System.Windows.Forms.Button buttonToA4Printer;
        private System.Windows.Forms.TextBox textBoxPageCountA1;
        private System.Windows.Forms.TextBox textBoxPageCountA2;
        private System.Windows.Forms.TextBox textBoxPageCountA3;
        private System.Windows.Forms.TextBox textBoxPageCountA4;
        private System.Windows.Forms.Label labelPageCount;
        private System.Windows.Forms.Button buttonPrintPdfFile;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem работаСПрограммойToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem путьКAdobeReaderToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openAdobeReaderFileDialog;
        private System.Windows.Forms.TextBox textBoxPageCountA0;
        private System.Windows.Forms.Button buttonToA0Printer;
        private System.Windows.Forms.TextBox textBoxA0Printer;
        private System.Windows.Forms.CheckBox checkBoxA0Printer;
        private System.Windows.Forms.CheckBox checkBoxA1Printer;
        private System.Windows.Forms.CheckBox checkBoxA2Printer;
        private System.Windows.Forms.CheckBox checkBoxA3Printer;
        private System.Windows.Forms.CheckBox checkBoxA4Printer;
        private System.Windows.Forms.ToolStripMenuItem активацияToolStripMenuItem;
    }
}

