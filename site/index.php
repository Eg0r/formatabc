
	<?php include ("header.php") ?>

	<div id="id_logo_left" class="logo_left">
		<img src="./images/abc.png" />
	</div>
	<div id="id_logo_right" class="logo_right">
		<img src="./images/abc.png" />
	</div>
	
	<div class="page_container">
		<div class="logo_header">
			<a href="/index.php"><img src="./images/abc.png" /></a>
		</div>
		<div class="printer_wrapper">
			<div class="printer">
				<img src="./images/printer.png" />
			</div>
		</div>
	
		<div class="page page_minus_3_deg page_deg">
		</div>
		<div class="page page_plus_17_deg page_deg">
		</div>
		<div class="page page_plus_17_deg_plus_top_100 page_deg">
		</div>
		<div class="page page_minus_20_deg page_deg">
		</div>
		<div class="page page_plus_17_deg_plus_top_1000 page_deg">
		</div>
		<div id="id_page" class="page">
			<div class="table top_table">
				<div class="table_row">
					<div class="table_cell align_bottom">
						<a href="./register_page.php"><button class="button button_left align_bottom button_grey">Регистрация</button></a>
					</div>
					<div class="table_cell align_bottom">
						<a href="/setup.exe"><button class="button button_right align_bottom button_red">Полная версия</button></a>
					</div>
				</div>
				<div class="table_row">
					<div class="table_cell align_middle">
						<a href="/setup.exe"><button class="button button_left align_middle button_red">Демо-версия</button></a>
					</div>
					<div class="table_cell align_middle">
						<a href="./pay.php"><button class="button button_right align_middle button_grey">Оплата <span style="color: red;">(1200руб.)</span></button></a>
					</div>
				</div>
			</div>
			<div class="head_text">
				О программе
			</div>
			<div class="simple_text paragraph">
				Программа <b>Format ABC</b> предназначена для организаций и частных лиц, которым
				необходимо быстро и правильно отправить на печать файл, имеющий разные форматы листов A0-A4. 
				Программа автоматически определяет количество форматов A0-A4 в файлах с расширением ".pdf" и отправляет на печать.
			</div>
			<div class="head_text">
				Возможности программы
			</div>
			<div class="table">
				<div class="table_row">
					<div class="table_cell">
						<img class="table_img" src="./images/hand_class.png" />
					</div>
					<div class="table_cell">
						<img class="table_img" src="./images/hand_class.png" />
					</div>
					<!--<div class="table_cell">
						<img class="table_img" src="./images/hand_class.png" />
					</div>-->
					<div class="table_cell">
						<img class="table_img" src="./images/hand_class.png" />
					</div>
				</div>
				<div class="table_row">
					<div class="table_cell center_text simple_text" style="max-width: 200px;">
						Автоматическое определение количества листов
					</div>
					<div class="table_cell center_text simple_text" style="max-width: 160px;">
						Автоматическое определение принтеров
					</div>
					<!--<div class="table_cell center_text simple_text" style="max-width: 200px;">
						Автоматическое определение склеенных листов
					</div>-->
					<div class="table_cell center_text simple_text" style="max-width: 240px;">
						Автоматическая отправка различных форматов на разные принтеры
					</div>
				</div>
			</div>
			<div class="head_text">
				По программе
			</div>
			<div class="simple_text center_text">
				Для работы с программой Вам необходимо:
			</div>
			<div class="table">
				<div class="table_row">
					<div class="table_cell center_text numerate">
						1
					</div>
					<div class="table_cell">
					</div>
					<div class="table_cell center_text numerate">
						2
					</div>
					<div class="table_cell">
					</div>
					<div class="table_cell center_text numerate">
						3
					</div>
					<div class="table_cell">
					</div>
					<div class="table_cell center_text numerate">
						4
					</div>
					<div class="table_cell">
					</div>
					<div class="table_cell center_text numerate">
						5
					</div>
				</div>
				<div class="table_row">
					<div class="table_cell">
						<img class="table_img" src="./images/register.png" />
					</div>
					<div class="table_cell top_img">
						<img class="table_img top_img" style="margin-left: -25px; margin-right: 15px;" src="./images/arrow.png" />
					</div>
					<div class="table_cell">
						<img class="table_img" src="./images/pay.png" />
					</div>
					<div class="table_cell">
						<img class="table_img reflect_img" src="./images/arrow.png" />
					</div>
					<div class="table_cell">
						<img class="table_img" src="./images/key.png" />
					</div>
					<div class="table_cell top_img">
						<img class="table_img" src="./images/arrow.png" />
					</div>
					<div class="table_cell">
						<img class="table_img" src="./images/download.png" />
					</div>
					<div class="table_cell">
						<img class="table_img reflect_img" src="./images/arrow.png" />
					</div>
					<div class="table_cell">
						<img class="table_img" src="./images/activate.png" />
					</div>
				</div>
				<div class="table_row">
					<div class="table_cell center_text">
						Зарегистрироваться
					</div>
					<div class="table_cell">
					</div>
					<div class="table_cell center_text">
						Оплатить
					</div>
					<div class="table_cell">
					</div>
					<div class="table_cell center_text">
						Получить ключ
					</div>
					<div class="table_cell">
					</div>
					<div class="table_cell center_text">
						Скачать программу
					</div>
					<div class="table_cell">
					</div>
					<div class="table_cell center_text">
						Активировать ключ
					</div>
				</div>
			</div>
			<div class="simple_text center_text">
				Для ознакомления с программой скачайте <a href="/setup.exe">ДЕМО-ВЕРСИЮ</a>
			</div>
			<div class="head_text">
				Помощь и поддержка
			</div>
			<div class="simple_text center_text">
				По возникшим вопросам просим Вас писать на адрес электронной почты <u>FormatABC@yandex.ru</u>
			</div>
		</div>
	</div>
	<script src="./js/position.js?5">
	</script>
	<div class="footer simple_text center_text">
		2016 </br>
		Все права защищены
	</div>
</body>
</html>