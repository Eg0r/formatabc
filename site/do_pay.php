<?php

require 'MySimplePayMerchant.class.php';

/**
 * Здесь представлен вариант с переадресацией сразу на страницу платежной системы
 * Установите payment_system = SP для переадресации на страницу выбора способа платежа
 * В случае если не будут указаны обязательные данные о плательщике, 
 * будет произведена переадресация на платежную страницу SimplePay для уточнения деталей
 */
 
include ("db.php");
	
	function randomPassword() 
	{
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}
	
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$pass = $_POST['pass'];
	
	$result = mysql_query("SELECT id FROM users WHERE email='$email'", $db);
    $myrow = mysql_fetch_array($result);

	if (empty($myrow['id'])) 
	{
		$pass = randomPassword() ;
		$result2 = mysql_query ("INSERT INTO users (email, pass, is_pay) VALUES('$email', '$pass', '0')");
		// Проверяем, есть ли ошибки
		if ($result2=='TRUE')
		{
			mail($email, "Регистрация FormatABC", "Здравствуйте! \n Вы были зарегистрированы на FormatABC \n Логин: '$email'\n Пароль: '$pass'", $headers);
			echo ("<span class=\"text_green\">Ваш email не зарегистрирован в системе. Была проведена автоматическая регистрация. Сгенерированный пароль отправлен Вам на почту.</span>");
		}
	}
	
	$result = mysql_query("SELECT id, pass, is_pay FROM users WHERE email='$email'", $db);
	$myrow = mysql_fetch_array($result);
	if ($pass != $myrow['pass'])
	{
		exit ("<span class=\"text_red\">Извините, введённый вами пароль неверный.</span>");
	}
	if ($myrow['is_pay'])
	{
		exit ("<span class=\"text_red\">Вы уже произвели оплату ранее.</span>");
	}
	
	$payment_data = new SimplePay_Payment;
	$payment_data->amount = 1200;
	$payment_data->order_id = $myrow['id'];
	$payment_data->client_email = $email;
	$payment_data->client_phone = $phone;
	$payment_data->description = 'Тест платежа';
	$payment_data->payment_system = 'TEST';
	$payment_data->client_ip = $_SERVER['REMOTE_ADDR'];

	/* Пример использования */

	// Создаем объект мерчант-класса SP
	$sp = new MySimplePayMerchant();

	/*$out = $sp->get_ps_list(100);
	print_output("Разрешенные платежные системы", $out);*/

	// Запрос на создание платежа
	$out = $sp->direct_payment($payment_data);
	//print_output("Информация об адресе для переадресации пользователя", $out);
	//print_r($out);

	// Получаем ссылку на платежную страницу
	$payment_link = $out['sp_redirect_url'];

	// Выводим ссылку
	echo '<span class=\"text_green\"><a href="' . $payment_link . '" target="_blank">Кликните для перехода на платёжную страницу</a></span>';

	// Запрос данных о созданном платеже
	//$out = $sp->get_order_status_by_order_id(1001);
	//print_output("Информация о созданном платеже", $out);

?>
