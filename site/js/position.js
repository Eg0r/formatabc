function adaptive()
{
	var leftLogo = document.getElementById("id_logo_left");
	var rightLogo = document.getElementById("id_logo_right");
	var page = document.getElementById("id_page");
	leftLogo.style.left = page.offsetLeft - leftLogo.offsetWidth - 12 + 'px';
	rightLogo.style.left = page.offsetLeft + page.offsetWidth + 12 + 'px';

	if (leftLogo.offsetLeft < 0)
	{
		leftLogo.style.display = 'none';
		rightLogo.style.display = 'none';
	}

	if (document.documentElement.clientWidth <= 1000)
	{
		var pagesDeg = document.getElementsByClassName("page_deg");
		for (var i = 0; i < pagesDeg.length; i++)
		{
			pagesDeg[i].style.display = 'none';
		}
	}
}

window.onload = function () { adaptive(); }