﻿using System;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Text.RegularExpressions;

namespace FormatABC
{
    public class RealDateTime
    {
        public static DateTime GetDateTime()
        {
            DateTime dateTime;
            string timeStamp = Request.POST("http://formatabc.ru/get_current_timestamp.php", "get_current_timestamp");
            dateTime = new DateTime(1970, 1, 1).AddSeconds(int.Parse(timeStamp)).ToLocalTime();
            return dateTime;
        }

        public static int GetTimeStamp()
        {
            return int.Parse(Request.POST("http://formatabc.ru/get_current_timestamp.php", "get_current_timestamp"));
        }
    }
}
