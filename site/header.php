<!DOCTYPE html>
<html lang="ru">
<head xmlns:og="http://ogp.me/ns#">
    <title>FormatABC</title> 
    <meta charset="utf-8">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="FormatABC" />
    <meta property="og:description" content="FormatABC - программа, которая предназначена для организаций и частных лиц, которым необходимо быстро и правильно отправить на печать файл, имеющий разные Форматы листов А0-А4. Программа автоматически определяет количество Форматов А0-А4 в файлах с расширением «.pdf» и отправляет на печать.
" />
    <meta property="og:image" content="./images/social.jpg">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="icon" type="image/x-icon" href="./images/">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
</head>
<body>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
		try {
		w.yaCounter41881099 = new Ya.Metrika({
		id:41881099,
		clickmap:true,
		trackLinks:true,
		accurateTrackBounce:true
		});
		} catch(e) { }
		});
		var n = d.getElementsByTagName("script")[0],
		s = d.createElement("script"),
		f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/watch.js";
		if (w.opera == "[object Opera]") {
		d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/41881099" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter --> 
	
	<div class="header">
	</div>