﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormatABC
{
    class Sizes
    {
        public static int A0wPt = 2384;
        public static int A0hPt = 3370;
        public static int A1wPt = 1684;
        public static int A1hPt = 2380;
        public static int A2wPt = 1190;
        public static int A2hPt = 1684;
        public static int A3wPt = 842;
        public static int A3hPt = 1190;
        public static int A4wPt = 595;
        public static int A4hPt = 842;

        public static string FORMAT_A0 = "A0";
        public static string FORMAT_A1 = "A1";
        public static string FORMAT_A2 = "A2";
        public static string FORMAT_A3 = "A3";
        public static string FORMAT_A4 = "A4";
        public static string FORMAT_OTHER = "other";

        public static int delta = 20;
    }
}
