﻿namespace FormatABC
{
    partial class ActivateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.textBoxActivateKey = new System.Windows.Forms.TextBox();
            this.buttonActivate = new System.Windows.Forms.Button();
            this.labelLogin = new System.Windows.Forms.Label();
            this.labelActivateKey = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPass = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.Location = new System.Drawing.Point(59, 12);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(139, 20);
            this.textBoxLogin.TabIndex = 0;
            // 
            // textBoxActivateKey
            // 
            this.textBoxActivateKey.Location = new System.Drawing.Point(59, 84);
            this.textBoxActivateKey.Name = "textBoxActivateKey";
            this.textBoxActivateKey.Size = new System.Drawing.Size(261, 20);
            this.textBoxActivateKey.TabIndex = 2;
            // 
            // buttonActivate
            // 
            this.buttonActivate.Location = new System.Drawing.Point(220, 119);
            this.buttonActivate.Name = "buttonActivate";
            this.buttonActivate.Size = new System.Drawing.Size(100, 23);
            this.buttonActivate.TabIndex = 2;
            this.buttonActivate.Text = "Активировать";
            this.buttonActivate.UseVisualStyleBackColor = true;
            this.buttonActivate.Click += new System.EventHandler(this.buttonActivate_Click);
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Location = new System.Drawing.Point(12, 15);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(35, 13);
            this.labelLogin.TabIndex = 3;
            this.labelLogin.Text = "Email:";
            // 
            // labelActivateKey
            // 
            this.labelActivateKey.AutoSize = true;
            this.labelActivateKey.Location = new System.Drawing.Point(12, 87);
            this.labelActivateKey.Name = "labelActivateKey";
            this.labelActivateKey.Size = new System.Drawing.Size(36, 13);
            this.labelActivateKey.TabIndex = 4;
            this.labelActivateKey.Text = "Ключ:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Пароль:";
            // 
            // textBoxPass
            // 
            this.textBoxPass.Location = new System.Drawing.Point(59, 47);
            this.textBoxPass.Name = "textBoxPass";
            this.textBoxPass.Size = new System.Drawing.Size(139, 20);
            this.textBoxPass.TabIndex = 1;
            // 
            // ActivateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 159);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxPass);
            this.Controls.Add(this.labelActivateKey);
            this.Controls.Add(this.labelLogin);
            this.Controls.Add(this.buttonActivate);
            this.Controls.Add(this.textBoxActivateKey);
            this.Controls.Add(this.textBoxLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ActivateForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Активация";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxLogin;
        private System.Windows.Forms.TextBox textBoxActivateKey;
        private System.Windows.Forms.Button buttonActivate;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.Label labelActivateKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPass;
    }
}