﻿using System;
using System.IO;
using System.Drawing.Printing;
using System.Windows.Forms;

using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using PdfSharp.Pdf.Printing;
using System.Net;

namespace FormatABC
{
    public partial class Form1 : Form
    {
        bool isRead;
        Settings settings;

        public Form1()
        {
            InitializeComponent();

            this.MaximizeBox = false;

            isRead = false;

            openPdfFileDialog.Filter = "PDF Files (*.pdf)|*.pdf";
            openAdobeReaderFileDialog.Filter = "EXE Files (*.exe)|*.exe";

            try
            {
                loadSettings();

                setAdobeReader();

                buttonRefreshPrinters_Click(this, null);

                checkExistPrinterName(textBoxA0Printer);
                checkExistPrinterName(textBoxA1Printer);
                checkExistPrinterName(textBoxA2Printer);
                checkExistPrinterName(textBoxA3Printer);
                checkExistPrinterName(textBoxA4Printer);

                checkLicense();

                активацияToolStripMenuItem.Enabled = false;
            }
            catch (DemoVersionLostException)
            {
                MessageBox.Show("Срок демо-версии программы закончен. Зарегистрируйте и перезапустите приложение.", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                buttonRefreshPrinters.Enabled = false;
                buttonLoadPdfFile.Enabled = false;
                buttonPrintPdfFile.Enabled = false;
                buttonToA0Printer.Enabled = false;
                buttonToA1Printer.Enabled = false;
                buttonToA2Printer.Enabled = false;
                buttonToA3Printer.Enabled = false;
                buttonToA4Printer.Enabled = false;
            }
            catch (DemoVersionIsContinueException)
            {
                MessageBox.Show("Вы используете демо-версию программы. Срок демо-версии 1 неделя!", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (WebException)
            {
                MessageBox.Show("Требуется подключение в интернету для совершения лицензирования продукта", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                buttonRefreshPrinters.Enabled = false;
                buttonLoadPdfFile.Enabled = false;
                buttonPrintPdfFile.Enabled = false;
                buttonToA0Printer.Enabled = false;
                buttonToA1Printer.Enabled = false;
                buttonToA2Printer.Enabled = false;
                buttonToA3Printer.Enabled = false;
                buttonToA4Printer.Enabled = false;
            }
        }

        private void checkLicense()
        {
            LicenseVersionChecker licenseVersionChecker = new LicenseVersionChecker();
            licenseVersionChecker.check();
        }

        private void setAdobeReader()
        {
            string[] paths = { Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + "\\Adobe\\Reader 9.0\\Reader\\AcroRd32.exe",
            Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + "\\Adobe\\Acrobat Reader DC\\Reader\\AcroRd32.exe",
            settings.iniObject.adobeReaderPath};
            PdfFilePrinter.AdobeReaderPath = "";
            for (int i = 0; i < paths.Length; i++)
            {
                if (File.Exists(paths[i]))
                {
                    PdfFilePrinter.AdobeReaderPath = paths[i];
                    break;
                }
            }
        }

        private void loadSettings()
        {
            settings = new Settings(Path.GetTempPath());
            /*textBoxA0Printer.Text = settings.iniObject.printeNameA0;
            textBoxA1Printer.Text = settings.iniObject.printeNameA1;
            textBoxA2Printer.Text = settings.iniObject.printeNameA2;
            textBoxA3Printer.Text = settings.iniObject.printeNameA3;
            textBoxA4Printer.Text = settings.iniObject.printeNameA4;
            checkBoxA0Printer.Checked = settings.iniObject.isPrintA0;
            checkBoxA1Printer.Checked = settings.iniObject.isPrintA1;
            checkBoxA2Printer.Checked = settings.iniObject.isPrintA2;
            checkBoxA3Printer.Checked = settings.iniObject.isPrintA3;
            checkBoxA4Printer.Checked = settings.iniObject.isPrintA4;*/
        }

        private void checkExistPrinterName(TextBox textBox)
        {
            bool isExistPrinterName = false;
            for (int i = 0; i < listBoxPrinters.Items.Count; i++)
            {
                if (textBox.Text.Equals(listBoxPrinters.Items[i]))
                {
                    isExistPrinterName = true;
                    break;
                }
            }
            if (!isExistPrinterName)
            {
                textBox.Text = "";
            }
        }

        private void buttonRefreshPrinters_Click(object sender, EventArgs e)
        {
            listBoxPrinters.Items.Clear();
            PrinterSettings.StringCollection sc = PrinterSettings.InstalledPrinters;
            for (int i = 0; i < sc.Count; i++)
            {
                listBoxPrinters.Items.Add(sc[i]);
            }
            if (listBoxPrinters.Items.Count == 0)
            {
                MessageBox.Show("Принтеры не найдены", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonPrintPdfFile_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = openPdfFileDialog.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                try
                {
                    PdfDocument document = PdfReader.Open(openPdfFileDialog.FileName, PdfDocumentOpenMode.Import);
                    PdfPages pages = document.Pages;

                    PdfDocument pdfA0 = new PdfDocument(Path.GetTempPath() + "\\tmpA0.pdf");
                    PdfDocument pdfA1 = new PdfDocument(Path.GetTempPath() + "\\tmpA1.pdf");
                    PdfDocument pdfA2 = new PdfDocument(Path.GetTempPath() + "\\tmpA2.pdf");
                    PdfDocument pdfA3 = new PdfDocument(Path.GetTempPath() + "\\tmpA3.pdf");
                    PdfDocument pdfA4 = new PdfDocument(Path.GetTempPath() + "\\tmpA4.pdf");
                    int otherCounter = 0;

                    for (int i = 0; i < pages.Count; i++)
                    {
                        
                        string format = calcFormat(pages[i]);
                        if (format == Sizes.FORMAT_A0)
                        {
                            pdfA0.AddPage(pages[i]);
                        }
                        else if (format == Sizes.FORMAT_A1)
                        {
                            pdfA1.AddPage(pages[i]);
                        }
                        else if (format == Sizes.FORMAT_A2)
                        {
                            pdfA2.AddPage(pages[i]);
                        }
                        else if (format == Sizes.FORMAT_A3)
                        {
                            pdfA3.AddPage(pages[i]);
                        }
                        else if (format == Sizes.FORMAT_A4)
                        {
                            pdfA4.AddPage(pages[i]);
                        }
                        else if (format == Sizes.FORMAT_OTHER)
                        {
                            Console.WriteLine(pages[i].Width + " " + pages[i].Height);
                            otherCounter++;
                        }
                    }
                    textBoxPageCountA0.Text = pdfA0.Pages.Count.ToString();
                    textBoxPageCountA1.Text = pdfA1.Pages.Count.ToString();
                    textBoxPageCountA2.Text = pdfA2.Pages.Count.ToString();
                    textBoxPageCountA3.Text = pdfA3.Pages.Count.ToString();
                    textBoxPageCountA4.Text = pdfA4.Pages.Count.ToString();

                    closePdfDocument(pdfA0);
                    closePdfDocument(pdfA1);
                    closePdfDocument(pdfA2);
                    closePdfDocument(pdfA3);
                    closePdfDocument(pdfA4);

                    isRead = true;
                }
                catch (Exception)
                {
                    MessageBox.Show("Не удаётся прочитать файл", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void closePdfDocument(PdfDocument pdfDocument)
        {
            if (pdfDocument.PageCount == 0)
            {
                pdfDocument.Dispose();
            }
            else
            {
                pdfDocument.Close();
            }
        }

        private bool checkSize(int w1, int h1, int w2, int h2, int delta)
        {
            bool isCheckSize;
            if (((w1 >= w2 - delta) && (w1 <= w2 + delta) && (h1 >= h2 - delta) && (h1 <= h2 + delta)) ||
                    ((w1 >= h2 - delta) && (w1 <= h2 + delta) && (h1 >= w2 - delta) && (h1 <= w2 + delta)))
            {
                isCheckSize = true;
            }
            else
            {
                isCheckSize = false;
            }
            return isCheckSize;
        }

        private string calcFormat(PdfPage page)
        {
            int width = (int)page.Width.Point;
            int height = (int)page.Height.Point;
            string format = Sizes.FORMAT_OTHER;
            if (checkSize(width, height, Sizes.A0wPt, Sizes.A0hPt, Sizes.delta))
            {
                format = Sizes.FORMAT_A0;
            }
            else if (checkSize(width, height, Sizes.A1wPt, Sizes.A1hPt, Sizes.delta))
            {
                format = Sizes.FORMAT_A1;
            }
            else if (checkSize(width, height, Sizes.A2wPt, Sizes.A2hPt, Sizes.delta))
            {
                format = Sizes.FORMAT_A2;
            }
            else if (checkSize(width, height, Sizes.A3wPt, Sizes.A3hPt, Sizes.delta))
            {
                format = Sizes.FORMAT_A3;
            }
            else if (checkSize(width, height, Sizes.A4wPt, Sizes.A4hPt, Sizes.delta))
            {
                format = Sizes.FORMAT_A4;
            }
            return format;
        }

        private void namePrinterToTextBox(TextBox textBox)
        {
            if (listBoxPrinters.SelectedItem != null)
            {
                textBox.Text = listBoxPrinters.SelectedItem.ToString();
            }
            else
            {
                MessageBox.Show("Выделите принтер в списке!", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void buttonToA0Printer_Click(object sender, EventArgs e)
        {
            namePrinterToTextBox(textBoxA0Printer);
        }

        private void buttonToA1Printer_Click(object sender, EventArgs e)
        {
            namePrinterToTextBox(textBoxA1Printer);
        }

        private void buttonToA2Printer_Click(object sender, EventArgs e)
        {
            namePrinterToTextBox(textBoxA2Printer);
        }

        private void buttonToA3Printer_Click(object sender, EventArgs e)
        {
            namePrinterToTextBox(textBoxA3Printer);
        }

        private void buttonToA4Printer_Click(object sender, EventArgs e)
        {
            namePrinterToTextBox(textBoxA4Printer);
        }

        private void printDocument(string printerName, bool isPrint, string documentName)
        {
            if (isPrint)
            {
                checkEmptyPrinterName(printerName);

                PdfFilePrinter pdfFilePrinter = new PdfFilePrinter(documentName, printerName);
                pdfFilePrinter.Print();
            }
        }

        private void buttonPrintPdfFile_Click_1(object sender, EventArgs e)
        {
            try
            {
                checkExistAdobeReaderPath();
                checkOpenedPdfFile();

                printDocument(textBoxA0Printer.Text, checkBoxA0Printer.Checked, Path.GetTempPath() + "\\tmpA0.pdf");
                printDocument(textBoxA1Printer.Text, checkBoxA1Printer.Checked, Path.GetTempPath() + "\\tmpA1.pdf");
                printDocument(textBoxA2Printer.Text, checkBoxA2Printer.Checked, Path.GetTempPath() + "\\tmpA2.pdf");
                printDocument(textBoxA3Printer.Text, checkBoxA3Printer.Checked, Path.GetTempPath() + "\\tmpA3.pdf");
                printDocument(textBoxA4Printer.Text, checkBoxA4Printer.Checked, Path.GetTempPath() + "\\tmpA4.pdf");
            }
            catch (PdfFileNotOpenedException)
            {
                MessageBox.Show("Pdf файл не выбран", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (PrinterNameEmptyException)
            {
                MessageBox.Show("Принтеры не выбраны", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (AdobeReaderPathNullException)
            {
                MessageBox.Show("Не указан путь к AdobeReader", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void checkExistAdobeReaderPath()
        {
            if ((PdfFilePrinter.AdobeReaderPath == null) || (PdfFilePrinter.AdobeReaderPath.Length == 0))
            {
                throw new AdobeReaderPathNullException();
            }
        }

        private void checkOpenedPdfFile()
        {
            if (!isRead)
            {
                throw new PdfFileNotOpenedException();
            }
        }

        private void checkEmptyPrinterName(string printerName)
        {
            if (printerName.Equals(string.Empty))
            {
                throw new PrinterNameEmptyException();
            }
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutForm form = new AboutForm();
            form.Show(this);
        }

        private void работаСПрограммойToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WorkWithProgrammForm form = new WorkWithProgrammForm();
            form.Show(this);
        }

        private void путьКAdobeReaderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = openAdobeReaderFileDialog.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                PdfFilePrinter.AdobeReaderPath = openAdobeReaderFileDialog.FileName;
                settings.iniObject.adobeReaderPath = openAdobeReaderFileDialog.FileName;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            settings.iniObject.isPrintA0 = checkBoxA0Printer.Checked;
            settings.iniObject.isPrintA1 = checkBoxA1Printer.Checked;
            settings.iniObject.isPrintA2 = checkBoxA2Printer.Checked;
            settings.iniObject.isPrintA3 = checkBoxA3Printer.Checked;
            settings.iniObject.isPrintA4 = checkBoxA4Printer.Checked;
            settings.iniObject.printeNameA0 = textBoxA0Printer.Text;
            settings.iniObject.printeNameA1 = textBoxA1Printer.Text;
            settings.iniObject.printeNameA2 = textBoxA2Printer.Text;
            settings.iniObject.printeNameA3 = textBoxA3Printer.Text;
            settings.iniObject.printeNameA4 = textBoxA4Printer.Text;
            settings.save();
        }

        private void активацияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActivateForm form = new ActivateForm();
            form.Show(this);
        }

        private void checkBoxA0Printer_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBoxA1Printer_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
