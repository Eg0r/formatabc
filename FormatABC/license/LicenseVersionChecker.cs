﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormatABC
{
    class LicenseVersionChecker
    {
        private Activator activator;

        public LicenseVersionChecker()
        {
            activator = new Activator();
        }

        public void check()
        {
            try
            {
                string email = activator.readEmail();
                string pass = activator.readPass();
                string license = activator.readLicense();

                if (!activator.checkLicense(email, pass, license))
                {
                    throw new LicenseLostException();
                }
            }
            catch (LicenseLostException)
            {
                int timeStamp = RealDateTime.GetTimeStamp();
                try
                {
                    int demoDate = activator.readDemoDate();
                    if (timeStamp - demoDate >= Activator.WEEK_DEMO_VERSION)
                    {
                        throw new DemoVersionLostException();
                    }
                    else
                    {
                        throw new DemoVersionIsContinueException();
                    }
                }
                catch (RegeditValueLostException)
                {
                    activator.writeDemoDateForFirstRun(timeStamp);
                    throw new DemoVersionIsContinueException();
                }
            }
        }
    }
}
