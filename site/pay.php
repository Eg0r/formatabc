<?php include ("header.php") ?>

	<div class="page_container_register">
		<div class="logo_header">
			<a href="/index.php"><img src="./images/abc.png" /></a>
		</div>
		<div id="id_page" class="page_register">
			<form class="form" method="post">
				<b><p class="text_center">Выполните вход и совершите оплату</p></b>
				<p>
					<label>Введите email указанный при регистрации:<br></label>
					<input class="input" id="email" type="text" size="15" maxlength="50">
				</p>
				<p>
					<label>Введите номер телефона для совершения оплаты:<br></label>
					<input class="input" id="phone" type="text" size="15" maxlength="20">
				</p>
				<p>
					<label>Введите пароль:<br></label>
					<input class="input" id="pass" type="password" size="15" maxlength="20">
				</p>
				<p>
					<button type="button" class="button align_bottom button_red" onclick="pay();">Оплатить</button>
				</p>
				<p>
					<div>Сумма к оплате: 1200руб.</div>
				</p>
				<p id="echo" style="display: none;">
				</p>
			</form>
		</div>
	</div>
	<script src="./js/position.js?5">
	</script>
	<script>
		function pay()
		{
			$.ajax(
			{
			  url: './do_pay.php',
			  type: "POST",
			  data: "email=" + $('#email').val() + "&pass=" + $('#pass').val() + "&phone=" + $("#phone").val(),
			  success: function(data) 
			  {
				$('#echo').show();
				$('#echo').html(data);
			  }
			});
		}
	</script>
	<div class="footer simple_text center_text">
		2016 </br>
		Все права защищены
	</div>
</body>
</html>